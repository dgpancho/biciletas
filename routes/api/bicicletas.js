var express = require('express');
var router = express.Router();
var bicicletasController = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletasController.bicicletas_list);
router.post('/create', bicicletasController.bicicletas_create);
router.post('/update', bicicletasController.bicicleta_update);
router.delete('/delete', bicicletasController.bicicletas_delete);

module.exports = router;