var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe("Testing Bicicletas", () => {
  beforeEach(function(done) {
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, {useNewUrlParser: true});

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function() {
      console.log('We are connected to test database');
      done();
    })
  });

  afterEach(function(done) {
    Bicicleta.deleteMany({}, function(err, success){
      if (err) console.log(err);
      done();
    });
  });

  describe("Bicicleta.createInstance", () => {
    it("Crea una instancia de Bicicleta", () => {
      var bici = Bicicleta.createInstance(1, 'rojo', 'urbana', [-0.184791, -78.483335]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe('rojo');
      expect(bici.modelo).toBe('urbana');
      expect(bici.ubicacion[0]).toEqual(-0.184791);
      expect(bici.ubicacion[1]).toEqual(-78.483335);
    });
  });

  describe("Bicicleta.allBicis", () => {
    it("Start empty", (done) => {
      Bicicleta.allBicis(function(err, bicis) {
        expect(bicis.length).toBe(0);
        done();
      })
    });
  });

  describe('Bicicletas.add', () => {
    it('agrega solo una bici', (done) => {
      // expect(Bicicleta.allBicis.length).toBe(0);
      var aBici = new Bicicleta({code: 1, color: 'rojo', modelo: 'urbana'});
      Bicicleta.add(aBici, function(err, newBici) {
        if(err) console.log(err);
        Bicicleta.allBicis(function(err, bicis){
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code);

          done();
        })
      })
    });
  });

  describe('Bicicleta.findByCode', () => {
    it('debe devolver la bici con code 1', (done) => {
      Bicicleta.allBicis(function(err, bicis){
        expect(bicis.length).toBe(0);

        var aBici = new Bicicleta({code: 1, color: 'verde', modelo: 'urbana'});
        Bicicleta.add(aBici, function(err, newBici) {
          if(err) console.log(err);
          
          var aBici1 = new Bicicleta({code: 2, color: 'roja', modelo: 'urbana'});
          Bicicleta.add(aBici1, function(err, newBici) {
            if(err) console.log(err);
            Bicicleta.findByCode(1, function(err, targetBici) {
              expect(targetBici.code).toBe(aBici.code);
              expect(targetBici.color).toBe(aBici.color);
              expect(targetBici.modelo).toBe(aBici.modelo);

              done();
            })
          });
        })
      });
    });
  });

  describe('Bicicleta.removeByCode', () => {
    it('eliminar bicicleta con code 1', (done) => {
      Bicicleta.allBicis(function(err, bicis) {
        expect(bicis.length).toBe(0);
        var aBici = new Bicicleta({code: 1, color: 'azul', modelo: 'urbana',});

        Bicicleta.add(aBici, (err, newBici) => {
          if(err) console.log(err);
          Bicicleta.removeByCode(1, (err) => {
            if(err) console.log(err);
            expect(bicis.length).toBe(0);
            done();
          });
        });
      });
    });
  });

});

// beforeEach(() => { Bicicleta.allBicis = []; });

// describe("Bicicleta.allBicis", () => {
//     it("comienza vacia", () => {
//       expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe("Bicicleta.add", () => {
//   it("Agregamos 1", () => {
//     expect(Bicicleta.allBicis.length).toBe(0);
//     var a = new Bicicleta(1, 'rojo', 'urbana', [-0.184791, -78.483335]);
//     Bicicleta.add(a)
//     expect(Bicicleta.allBicis.length).toBe(1);
//     expect(Bicicleta.allBicis[0]).toBe(a);
//   });
// });

// describe("Bicicleta.findById", () => {
//   it("debe devolver la bici con id 1", () => {
//     expect(Bicicleta.allBicis.length).toBe(0);
//     var aBici = new Bicicleta(1, 'verde', 'urbana', [-0.184791, -78.483335]);
//     var aBici2 = new Bicicleta(2, 'rojo', 'montaña', [-0.184791, -78.483335]);
//     Bicicleta.add(aBici);
//     Bicicleta.add(aBici2);
    
//     var targetBici = Bicicleta.findById(1);
//     expect(targetBici.id).toBe(1);
//     expect(targetBici.color).toBe(aBici.color);
//     expect(targetBici.modelo).toBe(aBici.modelo);
//   });
// });