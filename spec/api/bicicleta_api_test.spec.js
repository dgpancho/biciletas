var Bicicleta = require('../../models/bicicleta');
var mongoose = require('mongoose');
var request = require('request');
var server = require('../../bin/www');

var base_url = "http://localhost:5001/api/bicicletas";

// beforeAll(() => console.log('testeando...'));

describe('Bicicleta API', () => {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});
    
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
          console.log('We are connected to test database');
          done();
        })
      });
    
      afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
          if (err) console.log(err);
          done();
        });
      });

    describe('GET bicicletas/', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                console.log('RESULT',result)
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST bicicletas/create', () => {
        it('STATUS 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var aBici = '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat": -0.211249, "lng": -78.498034}'
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe('rojo');
                expect(bici.ubicacion[0]).toBe(-0.211249);
                expect(bici.ubicacion[1]).toBe(-78.498034);
                done();
            });
        });
    });

    describe("post bicicletas/delete", () => {
        it("status 204", (done) => {
            var headers = { "content-type": "application/json" };
            var aBici = '{"code":1}';
    
          request.delete(
            {
              headers: headers,
              url: base_url + "/delete",
              body: aBici,
            },
            function (error, eliminar) {
              done();
            }
          );
        });
    });
});