var Bicicleta = require('../../models/bicicleta');

exports.bicicletas_list = function(req, res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicletas_create = function(req, res){
    var bici = new Bicicleta(req.body.code, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici)
    res.status(200).json({  
        bicicleta: bici
    });
}

exports.bicicleta_update = function (req,res) {
    var bici = Bicicleta.findById(req.body.code);
    
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [ req.body.lat, req.body.lng];

    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicletas_delete = function(req, res) {
    Bicicleta.removeById(req.body.code);
    res.status(204).send();
}